﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using WindowsFormsApp1.BLL;
using WindowsFormsApp1.DTO;

namespace WindowsFormsApp1.GUI
{
    public partial class ChiSoDien : Form
    {
        

        public ChiSoDien()
        {
            InitializeComponent();
            

        }
        private ChisodienBLL chisodienBLL = new ChisodienBLL();
        private ChiSoDienDTO chisodienDTO = new ChiSoDienDTO();

        private void dataGridchisso_CellClick_1(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1) return;
           //if(dgvChiSoDien.Rows[e.RowIndex].Cells[e.ColumnIndex].Value !=null)
            int numrow;
            numrow = e.RowIndex;
            txtMaKH.Text = dgvChiSoDien.Rows[numrow].Cells[0].Value.ToString();
            txtMathang.Text = dgvChiSoDien.Rows[numrow].Cells[1].Value.ToString();
            txtChisocu.Text = dgvChiSoDien.Rows[numrow].Cells[2].Value.ToString();
            txtChisomoi.Text = dgvChiSoDien.Rows[numrow].Cells[3].Value.ToString();
        
        }
        private void ChiSoDien_Load(object sender, EventArgs e)
        {
        
        }

        private void btnNhap_Click(object sender, EventArgs e)
        {
              chisodienBLL.HienThi(dgvChiSoDien);
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            if (chisodienBLL.kiemtra(txtMaKH.Text) == true)
            {
                MessageBox.Show("Mã khách hàng đã tồn tại");
            }
            else
            {
                if (txtMaKH.Text.Trim() != "")
                {
                    //gán dữ liệu vào DTO
                    chisodienDTO.MaKh = txtMaKH.Text.ToLower();
                    chisodienDTO.MaThang = txtMathang.Text;
                    chisodienDTO.ChiSoCu = int.Parse(txtChisocu.Text);
                    chisodienDTO.ChiSoMoi = int.Parse(txtChisomoi.Text);
                    //gọi BLL thực hiện
                    chisodienBLL.Them(chisodienDTO);
                    //hiện lên dgv
                    chisodienBLL.HienThi(dgvChiSoDien);
                }
            }
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            if (txtMaKH.Text.Trim() != "")
            {
                //gán dữ liệu vào DTO
                chisodienDTO.MaKh = txtMaKH.Text.ToLower();
                chisodienDTO.MaThang = txtMathang.Text;
                chisodienDTO.ChiSoCu = int.Parse(txtChisocu.Text);
                chisodienDTO.ChiSoMoi = int.Parse(txtChisomoi.Text);
                //gọi BLL thực hiện
                chisodienBLL.Sua(chisodienDTO);
                //hiện lên dgv
                chisodienBLL.HienThi(dgvChiSoDien);
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (txtMaKH.Text.Trim() != "")
            {
                //gán dữ liệu vào DTO
                chisodienDTO.MaKh = txtMaKH.Text.ToLower();

                //gọi BLL thực hiện
                chisodienBLL.Xoa(chisodienDTO);
                //hiện lên dgv
                chisodienBLL.HienThi(dgvChiSoDien);
            }
        }

        private void btnTK_Click(object sender, EventArgs e)
        {
            if (txtMaKH.Text.Trim() != "")
            {
                //gán dữ liệu vào DTO
                chisodienDTO.MaKh = txtMaKH.Text.ToLower();
                //gọi BLL thực hiện
                var csdTim = chisodienBLL.TimKiem2(chisodienDTO, dgvChiSoDien);
                //khác null là tìm thấy, thực hiện bind lên ui
                if (csdTim != null)
                {
                    txtMaKH.Text = csdTim.MaKh;
                    txtMathang.Text = csdTim.MaThang;
                    txtChisocu.Text = csdTim.ChiSoCu.ToString();
                    txtChisomoi.Text = csdTim.ChiSoMoi.ToString();
                }
                else
                {
                    //không thấy thì xóa trăng
                    txtMaKH.Text = txtMathang.Text = txtChisocu.Text = txtChisomoi.Text = string.Empty;
                }
            }
        }

        private void dgvChiSoDien_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void btnPreview_Click(object sender, EventArgs e)
        {
            string path = Application.StartupPath + "\\xmlchisodien.xml";
            try
            {
                System.Diagnostics.Process.Start("browser.exe", path);
            }
            catch (Exception)
            {
                MessageBox.Show(this, "Chưa có file cần mở trong bin/debug", "thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
    }
}
