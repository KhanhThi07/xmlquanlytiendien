﻿
namespace WindowsFormsApp1.GUI
{
    partial class DangNhap
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GroupBox1 = new System.Windows.Forms.GroupBox();
            this.Panel2 = new System.Windows.Forms.Panel();
            this.txtMK = new System.Windows.Forms.TextBox();
            this.lbMK = new System.Windows.Forms.Label();
            this.Panel1 = new System.Windows.Forms.Panel();
            this.txtTenDN = new System.Windows.Forms.TextBox();
            this.lbTenDN = new System.Windows.Forms.Label();
            this.btnThoat = new System.Windows.Forms.Button();
            this.btnDangNhap = new System.Windows.Forms.Button();
            this.Label1 = new System.Windows.Forms.Label();
            this.GroupBox1.SuspendLayout();
            this.Panel2.SuspendLayout();
            this.Panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // GroupBox1
            // 
            this.GroupBox1.Controls.Add(this.Panel2);
            this.GroupBox1.Controls.Add(this.Panel1);
            this.GroupBox1.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GroupBox1.ForeColor = System.Drawing.Color.White;
            this.GroupBox1.Location = new System.Drawing.Point(15, 64);
            this.GroupBox1.Name = "GroupBox1";
            this.GroupBox1.Size = new System.Drawing.Size(359, 146);
            this.GroupBox1.TabIndex = 9;
            this.GroupBox1.TabStop = false;
            this.GroupBox1.Text = "Thông tin đăng nhập";
            // 
            // Panel2
            // 
            this.Panel2.Controls.Add(this.txtMK);
            this.Panel2.Controls.Add(this.lbMK);
            this.Panel2.Location = new System.Drawing.Point(7, 88);
            this.Panel2.Name = "Panel2";
            this.Panel2.Size = new System.Drawing.Size(345, 44);
            this.Panel2.TabIndex = 1;
            // 
            // txtMK
            // 
            this.txtMK.BackColor = System.Drawing.Color.DarkSlateGray;
            this.txtMK.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold);
            this.txtMK.ForeColor = System.Drawing.Color.White;
            this.txtMK.Location = new System.Drawing.Point(159, 8);
            this.txtMK.Name = "txtMK";
            this.txtMK.Size = new System.Drawing.Size(156, 26);
            this.txtMK.TabIndex = 2;
            this.txtMK.UseSystemPasswordChar = true;
            // 
            // lbMK
            // 
            this.lbMK.Location = new System.Drawing.Point(11, 11);
            this.lbMK.Name = "lbMK";
            this.lbMK.Size = new System.Drawing.Size(144, 22);
            this.lbMK.TabIndex = 0;
            this.lbMK.Text = "Mật khẩu";
            // 
            // Panel1
            // 
            this.Panel1.BackColor = System.Drawing.Color.DarkSlateGray;
            this.Panel1.Controls.Add(this.txtTenDN);
            this.Panel1.Controls.Add(this.lbTenDN);
            this.Panel1.Location = new System.Drawing.Point(7, 34);
            this.Panel1.Name = "Panel1";
            this.Panel1.Size = new System.Drawing.Size(344, 44);
            this.Panel1.TabIndex = 0;
            // 
            // txtTenDN
            // 
            this.txtTenDN.BackColor = System.Drawing.Color.DarkSlateGray;
            this.txtTenDN.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenDN.ForeColor = System.Drawing.Color.White;
            this.txtTenDN.Location = new System.Drawing.Point(158, 10);
            this.txtTenDN.Name = "txtTenDN";
            this.txtTenDN.Size = new System.Drawing.Size(156, 26);
            this.txtTenDN.TabIndex = 1;
            // 
            // lbTenDN
            // 
            this.lbTenDN.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTenDN.Location = new System.Drawing.Point(10, 11);
            this.lbTenDN.Name = "lbTenDN";
            this.lbTenDN.Size = new System.Drawing.Size(144, 22);
            this.lbTenDN.TabIndex = 0;
            this.lbTenDN.Text = "Tên đăng nhập";
            // 
            // btnThoat
            // 
            this.btnThoat.BackColor = System.Drawing.Color.White;
            this.btnThoat.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold);
            this.btnThoat.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.btnThoat.Location = new System.Drawing.Point(218, 224);
            this.btnThoat.Name = "btnThoat";
            this.btnThoat.Size = new System.Drawing.Size(105, 39);
            this.btnThoat.TabIndex = 11;
            this.btnThoat.Text = "Thoát";
            this.btnThoat.UseVisualStyleBackColor = false;
            this.btnThoat.Click += new System.EventHandler(this.btnThoat_Click);
            // 
            // btnDangNhap
            // 
            this.btnDangNhap.BackColor = System.Drawing.Color.White;
            this.btnDangNhap.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold);
            this.btnDangNhap.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.btnDangNhap.Location = new System.Drawing.Point(55, 224);
            this.btnDangNhap.Name = "btnDangNhap";
            this.btnDangNhap.Size = new System.Drawing.Size(105, 39);
            this.btnDangNhap.TabIndex = 10;
            this.btnDangNhap.Text = "Đăng nhập";
            this.btnDangNhap.UseVisualStyleBackColor = false;
            this.btnDangNhap.Click += new System.EventHandler(this.btnDangNhap_Click);
            // 
            // Label1
            // 
            this.Label1.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.ForeColor = System.Drawing.Color.White;
            this.Label1.Location = new System.Drawing.Point(10, 16);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(382, 41);
            this.Label1.TabIndex = 12;
            this.Label1.Text = "ĐĂNG NHẬP";
            this.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // DangNhap
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSlateGray;
            this.ClientSize = new System.Drawing.Size(399, 274);
            this.Controls.Add(this.GroupBox1);
            this.Controls.Add(this.btnThoat);
            this.Controls.Add(this.btnDangNhap);
            this.Controls.Add(this.Label1);
            this.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "DangNhap";
            this.Opacity = 0.8D;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DangNhap";
            this.Load += new System.EventHandler(this.DangNhap_Load);
            this.GroupBox1.ResumeLayout(false);
            this.Panel2.ResumeLayout(false);
            this.Panel2.PerformLayout();
            this.Panel1.ResumeLayout(false);
            this.Panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.GroupBox GroupBox1;
        internal System.Windows.Forms.Panel Panel2;
        internal System.Windows.Forms.TextBox txtMK;
        internal System.Windows.Forms.Label lbMK;
        internal System.Windows.Forms.Panel Panel1;
        internal System.Windows.Forms.TextBox txtTenDN;
        internal System.Windows.Forms.Label lbTenDN;
        internal System.Windows.Forms.Button btnThoat;
        internal System.Windows.Forms.Button btnDangNhap;
        internal System.Windows.Forms.Label Label1;
    }
}