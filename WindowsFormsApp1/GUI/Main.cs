﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1.GUI
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
        }

        private void QuảnLýToolStripMenuItem_Click(object sender, EventArgs e)
        {
           
        }

        private void HóaĐơnToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Hide();
            HoaDon frm = new HoaDon();
            frm.ShowDialog();
            Show();
        }

        private void KháchHàngToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Hide();
            QLKH frm = new QLKH();
            frm.ShowDialog();
            Show();
        }

        private void ChỉSốĐiệnToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Hide();
            ChiSoDien frm = new ChiSoDien();
            frm.ShowDialog();
            Show();
        }

        private void ThoátToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
