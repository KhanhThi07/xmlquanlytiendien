﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.IO;
using System.Windows.Forms;

namespace WindowsFormsApp1.BLL
{
    class xmlfile
    {
        public DataTable HienThi(string file)
        {
            DataTable dt = new DataTable();
            string FilePath = "C:/Users/ADMIN/OneDrive/Desktop/ProjectGitClone/quanlytiendien_xml/WindowsFormsApp1/" + file;
            if (File.Exists(FilePath))
            {
                FileStream fsReadXML = new FileStream(FilePath, FileMode.Open);
                dt.ReadXml(fsReadXML);
                fsReadXML.Close();
            }
            else
            {
                MessageBox.Show("File XML '" + file + "' không tồn tại");
            }

            return dt;
        }
    }
}
